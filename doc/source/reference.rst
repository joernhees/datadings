API reference
=============

.. toctree::
   :maxdepth: 6

   generated/datadings.commands
   generated/datadings.reader
   generated/datadings.sets
   generated/datadings.argparse
   generated/datadings.matlab
   generated/datadings.msgpack
   generated/datadings.tools
   generated/datadings.writer
