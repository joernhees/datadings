"""A number of useful tools are installed with datadings.
These will be accessible on the command line as ``datadings-*``
where ``*`` is replaced with one of the submodule names,

The main tool to interact with datadings in this way is
:py:mod:`datadings-write <datadings.commands.write>`.
It finds available datasets and runs their writing scripts.
"""
