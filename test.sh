#!/usr/bin/env bash
set -e -x

for PYBIN in /opt/python/*/bin; do
  (
    cd test
    "${PYBIN}/pip" install -r ../requirements.txt
    "${PYBIN}/pip" install -r ../test-requirements.txt
    "${PYBIN}/pip" install datadings --no-index -f ../dist --no-deps
    LIBDIR=$(
      "${PYBIN}/python" -c \
      "import os.path as pt; import datadings; print(pt.dirname(datadings.__file__))"
    )
    "${PYBIN}/python" -m pytest -vv
    "${PYBIN}/python" -m pytest --cov="${LIBDIR}" ./* --cov-report term-missing
  )
done
